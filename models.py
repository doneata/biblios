
from hello import db


class Book(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String())
    author = db.Column(db.String())
    year = db.Column(db.Integer)
    available = db.Column(db.Boolean)

    def __init__(self, title, author, year):
        self.title = title
        self.author = author
        try:
            self.year = int(year)
        except TypeError:
            self.year = None

    def __repr__(self):
        return '<Book %s>' % self.title

