
import os
import pdb

from flask.ext.sqlalchemy import SQLAlchemy
from flask import (
    Flask,
    render_template,
)


app = Flask(__name__)
app.config.from_object(os.environ['APP_SETTINGS'])
db = SQLAlchemy(app)


from models import *


@app.route('/')
def show_entries():
    books = Book.query.all()
    return render_template('show_books.html', books=books)


@app.route('/add_book', methods=['POST'])
def add_book():
    pass


if __name__ == '__main__':
    app.debug = True
    app.run()

