
## Updating the database

You can modify the schema of the database by changing the `models.py` file,
then run the migrations using the following commands:

```
#!bash
python manage.py db migrate
python manage.py db upgrade
```

The changes can be inspected by looking at the following:

* The migrations, see the `migrations` folder.
* The database, run `psql` and use the `\c` and `\d` commands to connect to the existing databases.

You can set a default value for a newly introduced column by using the `UPDATE` in `psql`.
Commit the changes and push them to `heroku`:

```
#!bash
git push heroku master
heroku run python manage.py db upgrade
heroku pg:psql
```
