
from hello import Book
from hello import db


def format_empty(field):
    return None if field == '---' else field


with open('books.txt', 'r') as ff:

    for line in ff.readlines():

        if '#' not in line:
            author, title, publisher, year = map(format_empty, line.strip().split(','))
        else:
            rest, title = line.strip().split('#')
            author, _, publisher, year = map(format_empty, rest.split(','))

        b = Book(title, author, year)
        db.session.add(b)

    db.session.commit()

